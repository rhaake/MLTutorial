import  ROOT
import  numpy as np

# HELPER FUNCTIONS
###########################################################################################
def LoadData(fileN, numSamples, offset = 0):
  """Load data from root tree"""

  # Create the data in a format that can be read
  data  = np.zeros(shape=(numSamples, 4 + 40*4 )) # we have 4 + kNumConst*4 features

  # Get raw input data
  inFile = ROOT.TFile(fileN)

  # Loop over all events and fill the samples selected
  # data into data
  p = 0
  skipped = 0
  for jet in inFile.jets:
    # Do jet selection here
    if skipped < offset:
      skipped += 1
      continue

    # Fill data
    data[p][0] = jet.recojet_m
    data[p][1] = jet.recojet_eta
    data[p][2] = jet.recojet_phi
    data[p][3] = jet.recojet_pt

    for iConst in range(min(jet.n_constituents, 40)):
      data[p][4+iConst+0] = jet.constituents_pt[iConst]
      data[p][4+iConst+1] = jet.constituents_eta[iConst]
      data[p][4+iConst+2] = jet.constituents_phi[iConst]
      data[p][4+iConst+3] = jet.constituents_id[iConst]

    p += 1
    if p >= numSamples:
      return data

  if p < numSamples:
    raise ValueError('Only {:d} samples loaded (requested = {:d}). Not enough samples?'.format(p, numSamples))

###########################################################################################
def LoadDataConstituents(fileN, numSamples, offset = 0):
  """Load data from root tree"""

  # Create the data in a format that can be read
  data  = np.zeros(shape=(numSamples, 40, 4)) # we have kNumConst*5 features

  # Get raw input data
  inFile = ROOT.TFile(fileN)

  # Loop over all events and fill the samples selected
  # data into data
  p = 0
  skipped = 0
  for jet in inFile.jets:
    # Do jet selection here
    if skipped < offset:
      skipped += 1
      continue

    for iConst in range(min(jet.n_constituents, 40)):
      data[p][iConst][0] = jet.constituents_pt[iConst]
      data[p][iConst][1] = jet.constituents_eta[iConst]
      data[p][iConst][2] = jet.constituents_phi[iConst]
      data[p][iConst][3] = jet.constituents_id[iConst]

    p += 1
    if p >= numSamples:
      return data

  if p < numSamples:
    raise ValueError('Only {:d} samples loaded (requested = {:d}). Not enough samples?'.format(p, numSamples))

###########################################################################################
def LoadTruth(fileN, numSamples, offset = 0):
  """Load truth from root tree"""

  # Create the data in a format that can be read
  data  = np.zeros(shape=(numSamples, 2)) # we have 1 truth value + 1 value for jet mass reco

  # Get raw input data
  inFile = ROOT.TFile(fileN)

  # Loop over all events and fill the samples selected
  # data into data
  p = 0
  skipped = 0
  for jet in inFile.jets:
    # Do jet selection here
    if skipped < offset:
      skipped += 1
      continue

    # Fill data
    data[p][0] = jet.genjet_m
    data[p][1] = jet.recojet_m

    p += 1
    if p >= numSamples:
      return data

  if p < numSamples:
    raise ValueError('Only {:d} samples loaded (requested = {:d}). Not enough samples?'.format(p, numSamples))
